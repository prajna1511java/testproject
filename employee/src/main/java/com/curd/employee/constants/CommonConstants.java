package com.curd.employee.constants;

import org.springframework.stereotype.Component;

@Component
public final class CommonConstants {

	public static final String ERROR = "Error";
	public static final String SUCCESS = "Success";
	
	public static final String EXCEPTION = "Exception";
	public static final String DATE_FORMAT_YYYY_MM_DD ="yyyy-MM-dd";
	
}

package com.curd.employee.dao.entity;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.LocalDate;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name ="EMPLOYEES")
public class Employee {

	@Id  
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "EMPLOYEE_ID" ,length = 6)
	private BigInteger employeeId;
	
	@Column(name = "FIRST_NAME", length = 20 , nullable = false)
	private String firstName;
	
	@Column(name = "LAST_NAME", length = 25, nullable = false)
	private String lastName;
	
	@Column(name = "EMAIL", length = 25)
	private String email;
	
	@Column(name = "PHONE_NUMBER",  length = 20)
	private String phoneNumber;
	
	@Column(name = "SALARY", nullable = false, precision = 8 , scale = 2)
	private BigDecimal salary;
	
	@Column(name = "HIRE_DATE", columnDefinition = "date")
	private LocalDate hireDate;
	
	@OneToOne(cascade = CascadeType.ALL,fetch = FetchType.EAGER)
	@JoinColumn(name = "DEPARTMENT_ID")
	private Department department;

	public BigInteger getEmployeeId() {
		return employeeId;
	}

	

	public void setEmployeeId(BigInteger employeeId) {
		this.employeeId = employeeId;
	}



	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public BigDecimal getSalary() {
		return salary;
	}

	public void setSalary(BigDecimal salary) {
		this.salary = salary;
	}

	public LocalDate getHireDate() {
		return hireDate;
	}

	public void setHireDate(LocalDate hireDate) {
		this.hireDate = hireDate;
	}



	public Department getDepartment() {
		return department;
	}



	public void setDepartment(Department department) {
		this.department = department;
	}
	
	
	
	
	
	
	
}

package com.curd.employee.dao;


import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.curd.employee.dao.entity.Employee;

@Service
public class EmployeeDao {
	
	@Autowired
	private EmployeeRepository employeeRepository;
	
	@Value("${employee.result.maxsize}")
	private int maxResultSize;
	
	public Employee getEmployeeById(BigInteger id) {
		
		employeeRepository.findByEmployeeId(id);
		return employeeRepository.findByEmployeeId(id);
	}
	
	public Employee saveEmployee(Employee emp) {
		return employeeRepository.save(emp);
	}

	public void deleteEmployee(Employee emp) {
		 employeeRepository.delete(emp);
	}
	
	public List<Employee> fetchAllEmployees(Integer pageNo,Integer pageSize, String sortBy){
	
		
		  Pageable paging = PageRequest.of(pageNo,pageSize,Sort.by(sortBy).descending());
		  
		  Page<Employee> employeePage= employeeRepository.findAll(paging);
		  
		  	if(employeePage.hasContent()) {
		  		return employeePage.getContent();
		  	}
		  	return new  ArrayList<Employee>();
		
		 
	}
		
}

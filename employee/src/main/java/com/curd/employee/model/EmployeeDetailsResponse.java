package com.curd.employee.model;

import java.util.ArrayList;
import java.util.List;

import com.curd.employee.dao.entity.Employee;


public class EmployeeDetailsResponse {

	private String transactionStatus;
	
	private List<Message> messages;
	
	private Employee employee;

	public String getTransactionStatus() {
		return transactionStatus;
	}

	public void setTransactionStatus(String transactionStatus) {
		this.transactionStatus = transactionStatus;
	}

	public List<Message> getMessages() {
		if(messages==null) {
			messages = new ArrayList<Message>();
		}
		return messages;
	}

	public void setMessages(List<Message> messages) {
		this.messages = messages;
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	
	
	
	
	
}

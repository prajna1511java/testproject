package com.curd.employee.model;

import java.math.BigInteger;

import javax.validation.constraints.Digits;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import com.curd.employee.constants.CommonConstants;
import com.curd.employee.validation.ValidDate;
import com.curd.employee.validation.ValidPhoneNumber;
import com.curd.employee.validation.ValidSalary;


public class EmployeeDetailsRequest {

	private BigInteger employeeId;
	
	@NotBlank(message = "First name is mandatory")
	@Size(min = 3,max = 20, message ="First name must be minimum of 2 characters" )
	private String firstName;
	
	@NotBlank(message = "Last name is mandatory")
	@Size(min = 3,max = 25, message ="Last name must be minimum of 2 characters" )
	private String lastName;
	
	@Size(max=25, message = "Email id is invalid")
	private String email;
	
	@ValidPhoneNumber
	private String phoneNumber;
	
	@ValidSalary
	@Digits(integer = 8, fraction = 2,message = "Invalid salary amount")
	private String salary;
	
	@ValidDate(message = CommonConstants.DATE_FORMAT_YYYY_MM_DD)
	private String hireDate;
	
	private DepartmentRequest department;

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getSalary() {
		return salary;
	}

	public void setSalary(String salary) {
		this.salary = salary;
	}

	public String getHireDate() {
		return hireDate;
	}

	public void setHireDate(String hireDate) {
		this.hireDate = hireDate;
	}

	public BigInteger getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(BigInteger employeeId) {
		this.employeeId = employeeId;
	}

	public DepartmentRequest getDepartment() {
		return department;
	}

	public void setDepartment(DepartmentRequest department) {
		this.department = department;
	}


	
	
}

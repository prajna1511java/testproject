package com.curd.employee.model;

import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import com.curd.employee.dao.entity.Employee;
import com.fasterxml.jackson.annotation.JsonIgnore;

public class DepartmentRequest {

	@NotBlank(message = "Depart name is manadortory")
	@Size(min = 3, max = 30)
	private String departmentName;
	
	@JsonIgnore
	private List<Employee> employees;

	public String getDepartmentName() {
		return departmentName;
	}

	public void setDepartmentName(String departmentName) {
		this.departmentName = departmentName;
	}

	public List<Employee> getEmployees() {
		if(employees == null) {
			employees = new ArrayList<Employee>();
		}
		return employees;
	}

	public void setEmployees(List<Employee> employees) {
		this.employees = employees;
	}
	
	
	
}

package com.curd.employee.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.apache.commons.lang3.StringUtils;

public class PhoneValidator implements ConstraintValidator<ValidPhoneNumber, String> {

	@Override
	public boolean isValid(String value, ConstraintValidatorContext context) {
		// TODO Auto-generated method stub
		if(StringUtils.isNotBlank(value) && value.matches("[0-9]+")  && value.length()<=20) {
			return true;
		}
		return false;
	}

}

package com.curd.employee.validation;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class DateValidator implements ConstraintValidator<ValidDate, String> {

	private  String format;
	@Override
	public boolean isValid(String value, ConstraintValidatorContext context) {
		// TODO Auto-generated method stub
		
		
		return isValid(value, format);
	}
	
	@Override
	public void initialize(ValidDate constraint) {
		format = constraint.format();
	}
	
	
	public boolean isValid(String value, String format) {
		
		try {
			DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(format);
			if(value!=null && !value.isEmpty()) {
				LocalDate.parse(value, dateTimeFormatter);
				return true;
			}
			return false;
		}catch (Exception e) {
			// TODO: handle exception
			return false;
		}
		
		
		
	}

	
	
	
}

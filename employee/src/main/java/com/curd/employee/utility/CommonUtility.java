package com.curd.employee.utility;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

@Component
public class CommonUtility {

	public LocalDate getDate(String date, String format) {
		
		if(StringUtils.isNotBlank(date)) {
			return LocalDate.parse(date, DateTimeFormatter.ofPattern(format));
		}
		return null;
	}
	
}

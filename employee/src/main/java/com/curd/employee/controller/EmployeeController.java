package com.curd.employee.controller;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.curd.employee.constants.CommonConstants;
import com.curd.employee.dao.EmployeeDao;
import com.curd.employee.dao.entity.Department;
import com.curd.employee.dao.entity.Employee;
import com.curd.employee.model.EmployeeDetailsRequest;
import com.curd.employee.model.EmployeeDetailsResponse;
import com.curd.employee.model.Message;
import com.curd.employee.utility.CommonUtility;
	
@RestController
public class EmployeeController {

	private static final Logger logger = LoggerFactory.getLogger(EmployeeController.class);
	@Autowired
	private EmployeeDao employeeDao;
	
	@Autowired
	private CommonUtility commonUtility;
	
	
	/**
	 * 
	 * Get employee details based on EmployeeID
	 * @param id
	 * @return
	 */
	
	@RequestMapping(value ="/employees/get/{id}", method = RequestMethod.GET)
	public ResponseEntity<EmployeeDetailsResponse> fetchEmployeeDetails(@PathVariable BigInteger id) {
		EmployeeDetailsResponse response = new EmployeeDetailsResponse();
		try {
			Employee emp =	employeeDao.getEmployeeById(id);
			
			if(emp != null) {
				
				response.setEmployee(emp);
				buildResponse(response, CommonConstants.SUCCESS, "FOUND", "Employee found");
				return ResponseEntity.status(HttpStatus.OK).body(response);
			}
				buildResponse(response, CommonConstants.ERROR, "NOT_FOUND", "Employee not found");
				return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response);
				
			 
		}catch (Exception e) {
			// TODO: handle exception
			buildResponse(response, CommonConstants.EXCEPTION, "Error", "Internal Server Error");
			 return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
		}
	}
	
	/**
	 * Fetch all employees based on EmployeeID
	 * @return
	 */
	@RequestMapping(value ="/employees/fetchall", method = RequestMethod.GET)
	public ResponseEntity<List<Employee>> fetchAllEmployeeDetails(@RequestParam(value = "pageNo") Integer pageNo,
			@RequestParam(value = "pageSize") Integer pageSize,@RequestParam(value = "sortBy") String sortBy) {
		List<Employee> employeeList = new ArrayList<Employee>();
		try {
			 employeeList =  employeeDao.fetchAllEmployees(pageNo,pageSize,sortBy);
			 return ResponseEntity.status(HttpStatus.OK).body(employeeList);	
		}catch (Exception e) {
			// TODO: handle exception
			 return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(employeeList);
		}
	}
	
	/**
	 * Add new employee
	 * @param employeeDetails
	 * @param bindingResult
	 * @return
	 */
	
	@RequestMapping(value = "/employees/add", method = RequestMethod.POST)
	public ResponseEntity<EmployeeDetailsResponse> createEmployeeDetails(@RequestBody @Valid EmployeeDetailsRequest employeeDetails, BindingResult bindingResult) {
		Employee employee = new Employee();
		EmployeeDetailsResponse response = new EmployeeDetailsResponse();
		try {
			logger.info(" Inside createEmployeeDetails method :");
			if(bindingResult.hasErrors()) {
				return validationError(bindingResult, response);
			}
			
			employee.setFirstName(employeeDetails.getFirstName());
			employee.setLastName(employeeDetails.getLastName());
			employee.setHireDate(commonUtility.getDate(employeeDetails.getHireDate(), CommonConstants.DATE_FORMAT_YYYY_MM_DD));
			employee.setEmail(employeeDetails.getEmail());
			employee.setPhoneNumber(employeeDetails.getPhoneNumber());
			employee.setSalary(new BigDecimal(employeeDetails.getSalary()));
			Department department = new Department();
			department.setDepartmentName(employeeDetails.getDepartment().getDepartmentName());
			employee.setDepartment(department); 
			Employee emp = employeeDao.saveEmployee(employee);
			response.setEmployee(emp);
			buildResponse(response,CommonConstants.SUCCESS,"ADDED","Employee sucessfully added");
			return ResponseEntity.status(HttpStatus.OK).body(response);
		}catch (Exception e) {
			// TODO: handle exception
			//Message message = new Message(messageType, code, description)
			buildResponse(response, CommonConstants.EXCEPTION, "Exception", "Internal Server Error");
			 return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
			
		}
		
	}

	private void buildResponse(EmployeeDetailsResponse response, String messageType, String code, String message) {
		response.setTransactionStatus(messageType);
		Message msg = new Message(messageType,code,message);
		response.getMessages().add(msg);
	}
	
	@RequestMapping(value = "/employees/update/{id}", method = RequestMethod.PUT)
	public ResponseEntity<EmployeeDetailsResponse> updateEmployeeDetails(@RequestBody @Valid EmployeeDetailsRequest employeeDetails, BindingResult bindingResult, 
			@PathVariable(value = "id") BigInteger employeeId) {
		Employee employee = new Employee();
		EmployeeDetailsResponse response = new EmployeeDetailsResponse();
		try {
			logger.info(" Inside createEmployeeDetails method :");
			if(bindingResult.hasErrors()) {
				return validationError(bindingResult, response);
			}
				
			employee = employeeDao.getEmployeeById(employeeId);
			
			if(employee != null) {
				employee.setFirstName(employeeDetails.getFirstName());
				employee.setLastName(employeeDetails.getLastName());
				employee.setHireDate(commonUtility.getDate(employeeDetails.getHireDate(), CommonConstants.DATE_FORMAT_YYYY_MM_DD));
				employee.setEmail(employeeDetails.getEmail());
				employee.setPhoneNumber(employeeDetails.getPhoneNumber());
				employee.setSalary(new BigDecimal(employeeDetails.getSalary()) );
				Department department = employee.getDepartment()!=null ? employee.getDepartment() : new Department();
				
				if(StringUtils.isNotBlank(employeeDetails.getDepartment().getDepartmentName())) {
					
					department.setDepartmentName(employeeDetails.getDepartment().getDepartmentName());
					
				}else {
					department.setDepartmentName(employee.getDepartment().getDepartmentName());
				}
				employee.setDepartment(department);
				employee = employeeDao.saveEmployee(employee);
				response.setEmployee(employee);
				buildResponse(response,CommonConstants.SUCCESS, "UPDATED", "Employee succesfully updated");
				return ResponseEntity.status(HttpStatus.OK).body(response);
			}
			buildResponse(response, CommonConstants.ERROR, "NOT_FOUND", "Employee not found");
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response);
			
		}catch (Exception e) {
			// TODO: handle exception
			//Message message = new Message(messageType, code, description)
			buildResponse(response, CommonConstants.EXCEPTION, "Exception", "Internal Server Error");
			 return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
			
		}
		
	}
	
	

	private ResponseEntity<EmployeeDetailsResponse> validationError(BindingResult bindingResult,
			EmployeeDetailsResponse response) {
		logger.info(" Inside validationError method :");
		response.setTransactionStatus(CommonConstants.ERROR);
		List<ObjectError> errors = bindingResult.getAllErrors();
		for(ObjectError error : errors) {
			Message msg = new Message("ERROR", error.getCode(), error.getDefaultMessage());
			response.getMessages().add(msg);
		}
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
	}
	
	@RequestMapping(value = "/employees/remove", method = RequestMethod.DELETE)
	public ResponseEntity<String> deleteEmployeeDetails(@RequestParam(value = "id") String employeeId) {
		try {
			if(StringUtils.isNotBlank(employeeId)) {
				Employee emp = employeeDao.getEmployeeById(new BigInteger(employeeId));
				if(emp!=null) {
					employeeDao.deleteEmployee(emp);	
					return ResponseEntity.status(HttpStatus.OK).body("Employee Id : "+employeeId+" is deleted");
				}
				return ResponseEntity.status(HttpStatus.OK).body("Employee not found");
				
			}
			
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Emoylee Id is mandtory");
					
		}catch (Exception e) {
			// TODO: handle exception
			 return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
		}
		
	} 
	

	
	
	
	
}
